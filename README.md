Projet quiz en HTML/CSS/Bootstrap/Typescript

https://quiz-musique.netlify.app/

Il y a une seule page et les elements se cachent et apparaissent en fonction de l'avancement du quiz.

1er affichage : choix du thème du quiz. 

Au clic, le bon style CSS et les questions du thème selectionné sont affichées (les choix cachés). 

J'ai mis 5 questions pour chaque thème.

Mes questions dans un tableau d'objet. Un objet contient la question, ses 4 réponses associées et la bonne réponse. 

Peu importe l'emplacement de la bonne réponse dans l'objet, elle peut être comparée avec la bonne réponse. 

Ajouter ou enlever des objets dans le tableau de questions n'aura pas d'impact sur le reste du code.

Il y a un timer pour chaque question, réglé à 15 secondes mais modifiable en argument. Une fois le temps écoulé ça passe à la prochaine question. (Juste pour ça il faudra modifier dans le HTML le temps par défaut que j'ai mis, si je l'enleve il y a un peu de temps sans qu'il y ai rien avant et j'aime pas le rendu).

Quand on clique sur la bonne réponse il n'y a pas besoin de valider ça va à la prochaine question. 

Il y a un effet lumineux rapide vert ou rouge autour du bloc du quiz en fonction de si la réponse est juste ou fausse.

Le score s'incremente en direct à chaque bonne réponse.

Quand on arrive à la fin des questions elles sont cachées et le score, les questions + les bonnes réponses et un bouton pour rafraichir la page sont affichés.