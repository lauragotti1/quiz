const questionText = document.querySelector<HTMLDivElement>('#questionText');
const answerA = document.querySelector<HTMLButtonElement>('#answerA');
const answerB = document.querySelector<HTMLButtonElement>('#answerB');
const answerC = document.querySelector<HTMLButtonElement>('#answerC');
const answerD = document.querySelector<HTMLButtonElement>('#answerD');
const resultPage = document.querySelector<HTMLElement>('#resultPage');
const correctAnswers = document.querySelector<HTMLElement>('#correctAnswers')
const questionContainer = document.querySelector<HTMLElement>('#questionContainer');
const retake = document.querySelector<HTMLButtonElement>('#retake');
const userScore = document.querySelector<HTMLSpanElement>('#userScore');
const scoreDiv = document.querySelector<HTMLDivElement>('.score');
const content = document.querySelector<HTMLDivElement>('.content');
const nirvana = document.querySelector<HTMLButtonElement>('#nirvana');
const clash = document.querySelector<HTMLButtonElement>('#clash');
const choices = document.querySelector<HTMLButtonElement>('#choices');
const body = document.querySelector<HTMLBodyElement>('body');
const totalScore = document.querySelector<HTMLSpanElement>('#totalScore')


let currentQuestion = 0; //compteur pour incrementer question
let score = 0; //compteur score
let choice = ""; //réponse sélectionnée

//CHOIX DU THEME 
//variable questions, vide jusqu'à choix du thème
let questions = [
    {
        question: "",
        a: "",
        b: "",
        c: "",
        d: "",
        ans: ""
    }
];


/**pour cacher les boutons de choix et afficher le quiz */
function afterChoice() {
    choices?.classList.add('hide');
    questionContainer?.classList.remove('hide');
    scoreDiv?.classList.remove('hide');
}

/**pour mettre dans le html le score max/total en fonction du nombre de questions */
function showTotalScore () {
    if(totalScore) {
    totalScore.innerHTML = "/ "+ questions.length;
    }
}

//choix du thème, au clique ça màj le tableau de question et applique le bon CSS + afterChoice() et showQ() 
nirvana?.addEventListener('click', () => {
    questions = [
        {
            question: "En quelle année est sorti le 1er album de Nirvana ?",
            a: '1989',
            b: '1991',
            c: '1993',
            d: '1994',
            ans: '1989'
        },
        {
            question: "Quel est le 1er titre de l’album In Utero ?",
            a: 'scentless apprentice',
            b: 'Serve the servants',
            c: 'heart shaped box',
            d: 'all apologies',
            ans: 'Serve the servants'
        },
        {
            question: "Sur quel label l’album Nevermind est sorti ?",
            a: 'Subpop',
            b: 'Geffen Records',
            c: 'DGC records',
            d: 'Rough Trade',
            ans: 'DGC records'
        },
        {
            question: 'En quelle année Dave Grohl rejoint le groupe ?',
            a: '1990',
            b: '1989',
            c: "1988",
            d: '1987',
            ans: '1990'
        },
        {
            question: 'Dans quelle ville le groupe s’est formé ?',
            a: 'Seattle',
            b: 'San Francisco',
            c: 'Aberdeen',
            d: 'New-York',
            ans: 'Aberdeen'
        }
    ];
    showQ();
    afterChoice();
    body?.classList.add('nirvanaStyle')
    showTotalScore();
}
)
clash?.addEventListener('click', () => {
    questions = [
        {
            question: "En quelle année The Clash se sont formés ?",
            a: '1975',
            b: '1976',
            c: '1977',
            d: '1978',
            ans: '1976'
        },
        {
            question: "Quel membre faisait parti des 101ers avant The Clash ?",
            a: 'Mick Jones',
            b: 'Joe Strummer',
            c: 'Paul Simonon',
            d: 'Topper Headon',
            ans: 'Joe Strummer'
        },
        {
            question: "Comment s'appelle le second album ?",
            a: 'Give em enough rope',
            b: 'London Calling',
            c: 'Sandinista',
            d: 'Combat Rock',
            ans: 'Give em enough rope'
        },
        {
            question: 'Quelle guitare Joe Strummer utilisait-il majoritairement ?',
            a: 'Stratocaster',
            b: 'Jazzmaster',
            c: 'Esquire',
            d: 'Telecaster',
            ans: 'Telecaster'
        },
        {
            question: "'I Fought The Law' est une reprise de quel artiste ?",
            a: 'The Crickets',
            b: 'Stray Cats',
            c: 'Bobby Fuller',
            d: "Ce n'est pas une reprise",
            ans: 'The Crickets'
        }
    ];
    showQ();
    afterChoice();
    body?.classList.add('clashStyle')
    showTotalScore();
}
)

//AFFICHAGE QUESTIONS + FONCTIONNEMENT QUIZ
/**pour afficher les questions/reponses puis les cacher quand c'est fini et afficher les bonnes réponses/score final*/
function showQ() {
    if (currentQuestion < questions.length) {
        startTimer(15);
        if (questionText) {
            questionText.innerHTML = questions[currentQuestion].question;
        }
        if (answerA) {
            answerA.textContent = questions[currentQuestion].a;
        }
        if (answerB) {
            answerB.innerHTML = questions[currentQuestion].b;
        }
        if (answerC) {
            answerC.innerHTML = questions[currentQuestion].c;
        }
        if (answerD) {
            answerD.innerHTML = questions[currentQuestion].d;
        }
    }
    else {
        console.log('fin');
        if (resultPage) {
            resultPage.classList.remove("hide");
        }
        if (correctAnswers) {
            correctAnswers.innerHTML = questions[0].question + "<p>Bonne réponse : " + questions[0].ans + "</p>" + questions[1].question +
                "<p>Bonne réponse : " + questions[1].ans + "</p>" + questions[2].question + "<p>Bonne réponse : " + questions[2].ans + "</p>" +
                questions[3].question + "<p>Bonne réponse : " + questions[3].ans + "</p>" + questions[4].question + "<p>Bonne réponse : " + questions[4].ans;
        }
        if (questionContainer) {
            questionContainer.classList.add("hide");
        }
    }
}

//events réponse selectionnée va dans variable choice et lance la fonction quiz
answerA?.addEventListener('click', () => {
    choice = questions[currentQuestion].a;
    quiz();
    startTimer(15)
})
answerB?.addEventListener('click', () => {
    choice = questions[currentQuestion].b;
    quiz();
    startTimer(15)
})
answerC?.addEventListener('click', () => {
    choice = questions[currentQuestion].c;
    quiz();
    startTimer(15)
})
answerD?.addEventListener('click', () => {
    choice = questions[currentQuestion].d;
    quiz();
    startTimer(15)
})

/**fonctionnement quiz, si c'est la bonne réponse (en fonction du choice qui est la valeur du bouton sur lequel on a cliqué) 
 * le score augmente, glow vert si correct ou rouge si faux, passe à la prochaine question en incrementant le compteur et en rappelant la fonction showQ*/
function quiz() {
    if (choice == questions[currentQuestion].ans) {
        console.log('true')
        score++;
        console.log("score =" + score)

        content?.classList.add('green-glow');
        setTimeout(function () { content?.classList.remove('green-glow') }, 300)

    } else {
        console.log('false')

        content?.classList.add('red-glow');
        setTimeout(function () { content?.classList.remove('red-glow') }, 300)
    }
    //met à jour le score dans le document
    if (userScore) {
        userScore.innerHTML = String(score);
    }
    //va à la prochaine question
    currentQuestion++;
    showQ();
}

//event sur le bouton recommencer le quiz : rafraichi la page 
retake?.addEventListener('click', () => {
    window.location.reload();
})


//timer
const timeCount = document.querySelector<HTMLSpanElement>('#timecount');
/**fonction timer, time = argument en nombre de secondes */
function startTimer(time:any) {
    let counter = setInterval(chrono, 1000);

    function chrono() {
        //addevents pour pas que le chrono se mette en doublon quand on clique avant la fin du chrono
        answerA?.addEventListener('click', () => {
            clearInterval(counter);
        });
        answerB?.addEventListener('click', () => {
            clearInterval(counter);
        });
        answerC?.addEventListener('click', () => {
            clearInterval(counter);
        });
        answerD?.addEventListener('click', () => {
            clearInterval(counter);
        });
        if (timeCount) {
            timeCount.textContent = time;
            time--;
            if (time < 9) {
                let addZero = timeCount.textContent;
                timeCount.textContent = "0" + addZero;
            }
            if (time < 0) {
                clearInterval(counter);
                //pour que le chrono s'arrete à la fin des questions
                if (currentQuestion < questions.length) {
                    quiz();
                }
            }
        }
    }

}






